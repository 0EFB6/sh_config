echo "DWM Keybins
M-N				: Tiling mode
M-M				: Floating mode
M-Comma				: Grid mode
M-Period			: Stacking mode
M-Slash				: Bottomstack mode
M-Z				: Focus next window
M-X				: Focus previous window
M-K				: Decrease window size 
M-J				: Increase window size
M-Shift-K			: Move window in anti-clockwise direction 
M-Shift-J			: Move window in clockwise direction 
M-C				: Closs focused window
M-B				: Toggle dwm bar
M-Tab				: Switch to previous workspace
M-Space				: Change window mode
M-I				: Focus next monitor
M-P				: Focus previous monitor

M-(1-9)				: Go to workspace 1-9
M-Shift-(1-9)			: Move selected window to workspace 1-9

M-Shift-Q			: Exit dwm

System
M-L				: Lock Screen
M-Shift-U			: Poweroff
M-Shift-R			: Reboot
M-Shift-N			: Hibernate
A-V				: Show Clipboard


Function Key
F1				: Decrease volume 
F3				: Increase volume
F6				: Lock Screen

Run prompt
M-R				: Run a program
M-A				: Show opened window
M-Shift-A			: Run Dmenu
M-Shift-F			: Show file browser

Keybinds for launching program
M-Enter				: Launch terminal
M-W				: Launch Chromium
M-Shift-W			: Launch Chromium (Incognito mode)
M-V				: Launch Firefox
M-Shift-V			: Launch Firefox (Incognito mode)
M-E				: Launch Pcmanfm
M-Shift-E			: Launch Vim File Explorer
M-S				: Launch Spotify
M-O				: Launch OBS
M-D				: Launch Discord
M-Q				: Launch Pulsemixer
M-Shift-Y			: Launch Stui
M-Shift-H			: Launch Htop
M-Shift-B			: Launch Btop
A-Shift-R			: Reload Sxhkd


Screenshot
PrintScreen			: Capture full screen
M-Y				: Capture selected area
"
