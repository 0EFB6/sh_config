static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c7dca8", "#1f350c" },
	[SchemeSel] = { "#c7dca8", "#739F4C" },
	[SchemeOut] = { "#c7dca8", "#9AAD62" },
};
