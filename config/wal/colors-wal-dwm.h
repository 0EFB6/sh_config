static const char norm_fg[] = "#c7dca8";
static const char norm_bg[] = "#1f350c";
static const char norm_border[] = "#8b9a75";

static const char sel_fg[] = "#c7dca8";
static const char sel_bg[] = "#7EC043";
static const char sel_border[] = "#c7dca8";

static const char urg_fg[] = "#c7dca8";
static const char urg_bg[] = "#739F4C";
static const char urg_border[] = "#739F4C";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
