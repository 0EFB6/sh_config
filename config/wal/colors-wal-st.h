const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#1f350c", /* black   */
  [1] = "#739F4C", /* red     */
  [2] = "#7EC043", /* green   */
  [3] = "#8BA930", /* yellow  */
  [4] = "#8CB34E", /* blue    */
  [5] = "#BABB47", /* magenta */
  [6] = "#9AAD62", /* cyan    */
  [7] = "#c7dca8", /* white   */

  /* 8 bright colors */
  [8]  = "#8b9a75",  /* black   */
  [9]  = "#739F4C",  /* red     */
  [10] = "#7EC043", /* green   */
  [11] = "#8BA930", /* yellow  */
  [12] = "#8CB34E", /* blue    */
  [13] = "#BABB47", /* magenta */
  [14] = "#9AAD62", /* cyan    */
  [15] = "#c7dca8", /* white   */

  /* special colors */
  [256] = "#1f350c", /* background */
  [257] = "#c7dca8", /* foreground */
  [258] = "#c7dca8",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
