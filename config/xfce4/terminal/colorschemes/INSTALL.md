### [xfce4-terminal](https://docs.xfce.org/apps/terminal/start)

#### Activating theme

1. Put `Dracula.theme` in `~/.local/share/xfce4/terminal/colorschemes` or `~/.config/xfce4/terminal/colorschemes`