#!/bin/bash

# Get memory usage percentage and format to 2 decimal places
memory_usage=$(free -h | grep Mem | awk '{print $3}')

# Output the formatted percentage
echo "$memory_usage"

