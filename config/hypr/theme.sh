#!/bin/bash

# Check if a theme argument is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <theme>"
  exit 1
fi

# Assign the theme parameter to a variable
theme="$1"

# Define the base config directory
config_dir="$HOME/.config/hypr"
config_dir_home='$HOME/.config/hypr/'
# Set the configs variable to the appropriate config directory
configs="config-$theme"

# Check if the config directory exists
if [ ! -d "$config_dir/$configs" ]; then
  echo "Error: The config directory '$config_dir/$configs' does not exist."
  exit 1
fi

# Update the main config file to point to the correct directory (relative path)
sed -i "s|^\$configs = .*|\$configs = $config_dir_home$configs|" "$config_dir/hyprland.conf"

# Set the symbolic links for hypridle.conf and hyprlock.conf
ln -sf "hypridle/$theme.conf" "$config_dir/hypridle.conf"
ln -sf "hyprlock/$theme.conf" "$config_dir/hyprlock.conf"

# Print success message
echo "Config setup completed for theme: $theme"
