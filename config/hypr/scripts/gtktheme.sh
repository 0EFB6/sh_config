#!/bin/bash

THEME='Dracula'
ICONS='Dracula'
FONT='Noto'
CURSOR='Dracula'
SCHEMA='gsettings set org.gnome.desktop.interface'
apply_theme() {
	${SCHEMA} gtk-theme "$THEME"
	${SCHEMA} icon-theme "$ICONS"
	${SCHEMA} cursor-theme "$CURSOR"
	${SCHEMA} font-name "$FONT"
	${SCHEMA} cursor-size 38
}
apply_theme
