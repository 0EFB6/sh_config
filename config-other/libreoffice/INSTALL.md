### [LibreOffice](https://www.libreoffice.org)

#### Activating palette

1. Copy `dracula.soc` to `~/.config/libreoffice/*/user/config/` (Linux) 
2. Choose the dracula palette when picking a color in LibreOffice.

#### Activating Application Colors (Linux and macOS only)

*Note that this is a bit experimental and might break your settings file. It
first saves a backup to* `registrymodifications.xcu.bak`, *which you can use if
anything goes wrong*.

1. Run `./add_dracula_application_colors.sh` to add the Dracula option to the
   settings file
2. Choose Dracula in *Tools -> Options -> Application Colors* (Linux)
