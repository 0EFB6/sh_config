# Dracula for [LibreOffice](https://www.libreoffice.org)

> A dark theme for [LibreOffice](https://www.libreoffice.org).

![Screenshot](./screenshot.png)

## License

[MIT License](./LICENSE)
